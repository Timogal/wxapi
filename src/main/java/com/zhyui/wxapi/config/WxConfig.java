package com.zhyui.wxapi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
@ConfigurationProperties(prefix = "wxapi")
@Configuration
public class WxConfig {

    private String key;
    private String secret;
    private String verifyToken;
    private String account;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getVerifyToken() {
        return verifyToken;
    }

    public void setVerifyToken(String verifyToken) {
        this.verifyToken = verifyToken;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
