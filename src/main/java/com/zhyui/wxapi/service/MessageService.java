package com.zhyui.wxapi.service;

import com.zhyui.wxapi.model.msg.BaseMessage;

public interface MessageService {

    BaseMessage onReceiveMessage(BaseMessage message);
}
