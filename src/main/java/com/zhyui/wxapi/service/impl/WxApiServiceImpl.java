package com.zhyui.wxapi.service.impl;

import com.zhyui.wxapi.config.WxConfig;
import com.zhyui.wxapi.model.MediaResp;
import com.zhyui.wxapi.service.WxApiService;
import com.zhyui.wxapi.util.JsonUtil;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/22
 */
@Service
@Deprecated
public class WxApiServiceImpl implements WxApiService {
    private static final ExecutorService ES = Executors.newCachedThreadPool();

    private static final String WX_API_URL = "https://api.weixin.qq.com/cgi-bin";

    private static final String TOKEN_REDIS_KEY = "wx:token";
    private static final String IMAGE_REDIS_KEY_PREFIX = "wx:image:";

    @Autowired
    private WxConfig wxConfig;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public MediaResp uploadImage(String remoteUrl) throws IOException {
        // 有缓存，直接返回
        String cached = getRemoteImageUrl(remoteUrl);
        if (cached != null) {
            return JsonUtil.from(cached, MediaResp.class, true);
        }
        ES.submit(() -> {
            try {
                URL url = new URL(remoteUrl);
                BufferedImage image = ImageIO.read(url);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                ImageIO.write(image, "jpeg", out);
                byte[] media = out.toByteArray();
                MediaResp uploaded = doUpload(media);
                storeRemoteImageUrl(remoteUrl, JsonUtil.toJson(uploaded, true));
            } catch (Exception e) {
                // do nothing
                e.printStackTrace();
            }
        });
        // 无缓存，返回空,进行下载操作
        return null;
    }

    @Override
    public MediaResp uploadImage(File image) throws IOException {
        return doUpload(image);
    }

    @Override
    public String getAccessToken() {
        String token = getTokenFromRedis();
        if (token != null) {
            return token;
        }
        return getTokenFromRemote();
    }

    /**
     * 上传操作
     *
     * @param media 要上传的文件
     * @return url
     * @throws IOException when upload failed
     */
    @SuppressWarnings("unchecked")
    private MediaResp doUpload(Object media) throws IOException {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = buildUploadBody(media);
        Request request = new Request.Builder()
                .url(WX_API_URL + "/media/upload")
                .post(requestBody).build();
        Response response = client.newCall(request).execute();
        if (response.body() == null) {
            throw new IOException("Upload Failed : " + response.message());
        }
        String s = response.body().string();
        Map<String, String> result = (Map<String, String>)
                JsonUtil.from(s, HashMap.class, false);
        if (result.containsKey("errcode")) {
            throw new IOException("Upload Failed : " + s);
        }
        String url = result.get("url");
        String mediaId = result.get("media_id");
        return new MediaResp(url, mediaId);
    }

    private RequestBody buildUploadBody(Object media) {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", "image")
                .addFormDataPart("access_token", getAccessToken());
        if (media instanceof byte[]) {
            builder.addFormDataPart("media", "media.jpg",
                    RequestBody.create(MultipartBody.FORM, (byte[]) media));
        } else if (media instanceof File) {
            File body = (File) media;
            builder.addFormDataPart("media", body.getName(),
                    RequestBody.create(MultipartBody.FORM, body));
        } else {
            throw new IllegalArgumentException("Unsupported Media : " + media.getClass().getName());
        }
        return builder.build();
    }

    /**
     * getAccessToken
     *
     * @return accessToken
     */
    @SuppressWarnings("unchecked")
    private String getTokenFromRemote() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(WX_API_URL + "/token?"
                        + String.format("grant_type=client_credential&appid=%s&secret=%s",
                        wxConfig.getKey(), wxConfig.getSecret()))
                .get().build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() == null) {
                throw new RuntimeException("Get token failed");
            }
            String s = response.body().string();
            Map<String, Object> result = (Map<String, Object>)
                    JsonUtil.from(s, HashMap.class, false);
            if (!result.containsKey("errcode")) {
                String token = Objects.toString(result.get("access_token"));
                storeToken(token);
                return token;
            }
            throw new IOException("get Token failed : " + s);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /***  Cache Operations ***/

    private void storeRemoteImageUrl(String origin, String result) {
        // ignore protocol
        final String key = origin.replace("http://", "")
                .replace("https://", "");
        byte[] keyBytes = ((StringRedisSerializer) redisTemplate.getKeySerializer())
                .serialize(IMAGE_REDIS_KEY_PREFIX + key);
        byte[] valueBytes = ((StringRedisSerializer) redisTemplate.getValueSerializer())
                .serialize(result);
        redisTemplate.execute((RedisCallback<String>) connection -> {
            connection.set(keyBytes, valueBytes);
            return null;
        });
    }

    private String getRemoteImageUrl(String origin) {
        // ignore protocol
        final String key = IMAGE_REDIS_KEY_PREFIX + origin.replace("http://", "")
                .replace("https://", "");
        byte[] keyBytes = ((StringRedisSerializer) redisTemplate.getKeySerializer())
                .serialize(key);
        return getRedisValue(keyBytes);
    }

    private void storeToken(String token) {
        byte[] key = ((StringRedisSerializer) redisTemplate.getKeySerializer())
                .serialize(TOKEN_REDIS_KEY);
        byte[] tokenBytes = ((StringRedisSerializer) redisTemplate.getValueSerializer())
                .serialize(token);
        redisTemplate.execute((RedisCallback<String>) connection -> {
            connection.setEx(key, 7200, tokenBytes);
            return null;
        });
    }

    private String getTokenFromRedis() {
        byte[] key = ((StringRedisSerializer) redisTemplate.getKeySerializer())
                .serialize(TOKEN_REDIS_KEY);
        return getRedisValue(key);
    }

    private String getRedisValue(byte[] key) {
        return redisTemplate.execute((RedisCallback<String>) connection -> {
            byte[] result = connection.get(key);
            if (result == null) {
                return null;
            }
            return (String) redisTemplate.getValueSerializer().deserialize(result);
        });
    }

    @PreDestroy
    public void destroy() {
        ES.shutdown();
    }
}
