package com.zhyui.wxapi.service.impl;

import com.zhyui.wxapi.config.WxConfig;
import com.zhyui.wxapi.model.SentenceResp;
import com.zhyui.wxapi.model.WallpaperResp;
import com.zhyui.wxapi.model.msg.BaseMessage;
import com.zhyui.wxapi.model.msg.EventMessage;
import com.zhyui.wxapi.model.msg.ImageMessage;
import com.zhyui.wxapi.model.msg.TextMessage;
import com.zhyui.wxapi.service.MessageService;
import com.zhyui.wxapi.util.JsonUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
@Service
public class MessageServiceImpl implements MessageService {

    private static final String SENTENCE_1 = "鸡汤";
    private static final String SENTENCE_2 = "金句";
    private static final String WALLPAPER = "壁纸";

    private static final String SENTENCE_API = "http://open.iciba.com/dsapi";
    private static final String WALLPAPER_API = "https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=zh-CN";
    private static final String WALLPAPER_BASE = "https://cn.bing.com";

    @Autowired
    private WxConfig wxConfig;
//    @Autowired
//    private WxApiService wxApiService;

    @Override
    public BaseMessage onReceiveMessage(BaseMessage message) {
        if (TextMessage.TYPE.equals(message.getMsgType())) {
            return replyTextMsg((TextMessage) message);
        }
        if (EventMessage.TYPE.equals(message.getMsgType())) {
            return replyEventMessage((EventMessage) message);
        }
        return defaultMessage(message.getFromUserName());
    }

    private BaseMessage replyTextMsg(TextMessage textMessage) {
        String content = textMessage.getContent();
        content = content.trim();
        switch (content) {
            case SENTENCE_1:
            case SENTENCE_2:
                return fetchDailySentence(textMessage.getFromUserName());
            case WALLPAPER:
                return fetchWallpaper(textMessage.getFromUserName());
            default:
                return defaultMessage(textMessage.getFromUserName());
        }
    }

    private TextMessage replyEventMessage(EventMessage eventMessage) {
        String event = eventMessage.getEvent();
        switch (event) {
            case "subscribe":
                return buildMessage(eventMessage.getFromUserName(),
                        "感谢您的关注！\n" +
                                "回复\"鸡汤\"或\"金句\"获取每日金句\n回复\"壁纸\"获取今日美图");
            case "unsubscribe":
                return buildMessage(eventMessage.getFromUserName(), "欢迎下次订阅");
            default:
                throw new UnsupportedOperationException("不支持的事件类型");
        }
    }

    /**
     * 获取每日金句
     *
     * @param receiver 消息接受者
     * @return
     */
    private TextMessage fetchDailySentence(String receiver) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(SENTENCE_API)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                String s = response.body().string();
                SentenceResp resp = JsonUtil.from(s, SentenceResp.class, true);
                return buildMessage(receiver,
                        resp.getContent()
                                + "\n\n"
                                + resp.getNote());
            }
            throw new RuntimeException("request failed");
        } catch (Exception e) {
            return failedMessage(receiver);
        }
    }

    private BaseMessage fetchWallpaper(String receiver) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(WALLPAPER_API)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                String s = response.body().string();
                WallpaperResp resp = JsonUtil.from(s, WallpaperResp.class, true);
                List<WallpaperResp.Image> images = resp.getImages();
                if (images != null && !images.isEmpty()) {
                    WallpaperResp.Image image = images.get(0);
                    String url = WALLPAPER_BASE + image.getUrl();
//                    MediaResp mediaResp = wxApiService.uploadImage(url);
//                    if (mediaResp == null) {
                    return buildMessage(receiver,
                            "时间：" + image.getEnddate() +
                                    "\n描述（作者）：" + image.getCopyright()
                                    + "\n原图：" + url);
//                    }
//                    return buildImageMessage(mediaResp.getMediaId(), receiver);
                }
            }
            throw new RuntimeException("request failed");
        } catch (Exception e) {
            e.printStackTrace();
            return failedMessage(receiver);
        }
    }

    private TextMessage failedMessage(String receiver) {
        return buildMessage(receiver, "系统内部错误");
    }

    private TextMessage defaultMessage(String receiver) {
        return buildMessage(receiver, "回复\"鸡汤\"或\"金句\"获取每日金句\n回复\"壁纸\"获取今日美图");
    }

    @Deprecated
    private ImageMessage buildImageMessage(String mediaId, String receiver) {
        ImageMessage im = new ImageMessage();
        im.setMediaId(mediaId);
        im.setFromUserName(wxConfig.getAccount());
        im.setToUserName(receiver);
        im.setCreateTime(System.currentTimeMillis());
        return im;
    }

    private TextMessage buildMessage(String receiver, String content) {
        TextMessage msg = new TextMessage();
        msg.setContent(content);
        msg.setCreateTime(System.currentTimeMillis());
        msg.setFromUserName(wxConfig.getAccount());
        msg.setToUserName(receiver);
        return msg;
    }
}
