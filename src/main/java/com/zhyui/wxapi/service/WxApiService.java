package com.zhyui.wxapi.service;

import com.zhyui.wxapi.model.MediaResp;

import java.io.File;
import java.io.IOException;

/**
 * Generate by IDE, pls modify this.
 * <p>
 * 微信不能回复图片消息
 *
 * @deprecated
 */
@Deprecated
public interface WxApiService {

    /**
     * 远程图片上传
     *
     * @param remoteUrl remoteUrl
     * @return uploaded url
     * @throws IOException exception
     */
    MediaResp uploadImage(String remoteUrl) throws IOException;

    MediaResp uploadImage(File image) throws IOException;

    /**
     * 获取微信的accessToken
     *
     * @return accessToken
     */
    String getAccessToken();
}
