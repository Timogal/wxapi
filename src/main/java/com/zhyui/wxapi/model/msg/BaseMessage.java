package com.zhyui.wxapi.model.msg;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
public class BaseMessage {

    private String toUserName;
    private String fromUserName;
    private Long createTime;
    private String content;
    private String msgId;
    private String msgType;

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    @Override
    public String toString() {
        return "BaseMessage{" +
                "ToUserName='" + toUserName + '\'' +
                ", FromUserName='" + fromUserName + '\'' +
                ", CreateTime=" + createTime +
                ", Content='" + content + '\'' +
                ", MsgId='" + msgId + '\'' +
                ", MsgType='" + getMsgType() + '\'' +
                '}';
    }
}
