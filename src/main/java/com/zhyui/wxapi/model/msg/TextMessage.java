package com.zhyui.wxapi.model.msg;

import com.zhyui.wxapi.model.msg.BaseMessage;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
@XmlRootElement(name = "xml")
public class TextMessage extends BaseMessage {
    public static final String TYPE = "text";

    @Override
    public String getMsgType() {
        return TYPE;
    }
}
