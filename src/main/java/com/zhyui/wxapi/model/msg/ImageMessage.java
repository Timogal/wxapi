package com.zhyui.wxapi.model.msg;

/**
 * 微信不让回复图片消息
 *
 * @deprecated
 */
@Deprecated
public class ImageMessage extends BaseMessage {
    public static final String TYPE = "image";

    private String mediaId;

    @Override
    public String getMsgType() {
        return TYPE;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
