package com.zhyui.wxapi.model.msg;

public class EventMessage extends BaseMessage {

    public static final String TYPE = "event";

    private String event;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    @Override
    public String getMsgType() {
        return TYPE;
    }
}
