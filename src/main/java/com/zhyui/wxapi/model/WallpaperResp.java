package com.zhyui.wxapi.model;

import java.util.List;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/22
 */
public class WallpaperResp {

    private List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public static class Image {
        private String enddate;
        private String url;
        private String copyright;

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCopyright() {
            return copyright;
        }

        public void setCopyright(String copyright) {
            this.copyright = copyright;
        }
    }
}
