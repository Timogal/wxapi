package com.zhyui.wxapi.model;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/22
 */
public class MediaResp {

    private String url;
    private String mediaId;

    public MediaResp() {
    }

    public MediaResp(String url, String mediaId) {
        this.url = url;
        this.mediaId = mediaId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
