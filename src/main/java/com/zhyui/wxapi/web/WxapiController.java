package com.zhyui.wxapi.web;

import com.zhyui.wxapi.config.WxConfig;
import com.zhyui.wxapi.model.msg.BaseMessage;
import com.zhyui.wxapi.service.MessageService;
import com.zhyui.wxapi.util.SecurityUtil;
import com.zhyui.wxapi.util.XmlUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
@RestController
public class WxapiController {

    @Autowired
    private WxConfig wxConfig;
    @Autowired
    private MessageService messageService;

    @GetMapping(value = {"/wx", "/", ""})
    public ResponseEntity<String> check(
            @RequestParam String timestamp,
            @RequestParam String nonce,
            @RequestParam String signature,
            @RequestParam String echostr) {
        String[] array = new String[]{nonce, timestamp, wxConfig.getVerifyToken()};
        Arrays.sort(array);
        String encodingStr = StringUtils.join(array, "");
        String sign = SecurityUtil.sha1(encodingStr);
        if (sign.equals(signature)) {
            return ResponseEntity.ok(echostr);
        }
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body("invalid");
    }

    @PostMapping(value = {"", "/wx", "/"},
            consumes = MediaType.TEXT_XML_VALUE,
            produces = MediaType.TEXT_XML_VALUE)
    public ResponseEntity<String> onReceiveMessage(@RequestBody String xml) {
        BaseMessage message = XmlUtil.parseMessage(xml);
        BaseMessage reply = messageService.onReceiveMessage(message);
        LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "text/xml");
        String result = XmlUtil.toXml(reply, true);
        result = result.replaceAll(reply.getClass().getSimpleName(), "xml");
        return new ResponseEntity<>(
                result,
                headers,
                HttpStatus.OK);
    }
}
