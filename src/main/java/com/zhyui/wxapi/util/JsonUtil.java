package com.zhyui.wxapi.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import java.io.IOException;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
public class JsonUtil {

    public static <T> T from(String json, Class<T> clazz, boolean useSnakeCase) {
        ObjectMapper objectMapper = new ObjectMapper();
        if (useSnakeCase) {
            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        }
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return objectMapper.readValue(json, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toJson(Object obj, boolean useSnakeCase) {
        ObjectMapper objectMapper = new ObjectMapper();
        if (useSnakeCase) {
            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        }
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
