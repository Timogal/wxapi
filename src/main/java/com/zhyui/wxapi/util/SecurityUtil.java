package com.zhyui.wxapi.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
public class SecurityUtil {

    public static String sha1(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(input.getBytes());
            byte[] output = digest.digest();
            return toHexString(output);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static String toHexString(byte[] data) {
        StringBuilder sb = new StringBuilder();
        String hex;
        for (byte b : data) {
            hex = Integer.toString((b & 0xff) + 0x100, 16).substring(1);
            sb.append(hex);
        }
        return sb.toString();
    }
}
