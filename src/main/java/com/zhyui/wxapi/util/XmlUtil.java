package com.zhyui.wxapi.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.zhyui.wxapi.model.msg.BaseMessage;
import com.zhyui.wxapi.model.msg.EventMessage;
import com.zhyui.wxapi.model.msg.TextMessage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Generate by IDE, pls modify this.
 *
 * @author LiSiJie @ 2018/5/21
 */
public class XmlUtil {

    private static final String MSG_TYPE_KEY = "MsgType";

    public static <T> T parse(String xml, Class<T> clazz, boolean useUpperCamelCase) {
        XmlMapper xmlMapper = createMapper(useUpperCamelCase);
        try {
            return xmlMapper.readValue(xml, clazz);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static BaseMessage parseMessage(String xml) {
        Map<String, String> xmlMap = parse(xml, HashMap.class, true);
        String type = xmlMap.get(MSG_TYPE_KEY);
        switch (type) {
            case TextMessage.TYPE:
                return parse(xml, TextMessage.class, true);
            case EventMessage.TYPE:
                return parse(xml, EventMessage.class, true);
            default:
                return null;
        }
    }

    public static String toXml(Object instance, boolean useUpperCamelCase) {
        XmlMapper xmlMapper = createMapper(useUpperCamelCase);
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return xmlMapper.writeValueAsString(instance);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private static XmlMapper createMapper(boolean useUpperCamelCase) {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        if (useUpperCamelCase) {
            xmlMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        }
        return xmlMapper;
    }
}
