package com.zhyui.wxapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class WxapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxapiApplication.class, args);
    }
}
