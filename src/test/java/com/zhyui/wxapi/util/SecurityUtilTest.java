package com.zhyui.wxapi.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class SecurityUtilTest {

    @Test
    public void sha1() {
        String output = SecurityUtil.sha1("abc");
        assertEquals("a9993e364706816aba3e25717850c26c9cd0d89d", output);
    }
}